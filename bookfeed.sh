#!/bin/bash
cat header.tex
for f in *.md
do
    pandoc -f markdown -t latex "$f"
done
cat footer.tex
