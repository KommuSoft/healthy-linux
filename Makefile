all : book.pdf clean
book.pdf : *.md header.tex footer.tex Makefile *.sh
	bash makebook.sh
clean :
	rm -f *.log *.aux *.toc
	
