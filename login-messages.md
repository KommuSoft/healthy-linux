# Customize linux login messages

When you perform a Linux login, the terminal will display a few messages like:

 > Welcome to Ubuntu 14.04.3 LTS (GNU/Linux 3.13.0-62-generic i686)

evidently these messages are generated somewhere. This is interesting because one can select which messages to show as well as generating other messages (with or without dynamic content).

The executables that generated these messages are located at `/etc/update-motd.d`. All files in the directory are owned by `root`. The folder contains files like: `00-header`, `10-help-text`, `90-updates-available`, `91-release-upgrade`, `98-fsck-at-reboot`, `98-reboot-required`. Without going into detail about what each program does, the leading numbers define an order in the fules, such that - given the files are  executed in alphabetical order - the `00-header` file is executed before the `98-fsck-at-reboot` file.

## Disabling messages

You can disable messages by resetting the execution bit. For instance:

    sudo chmod -x '00-header'

will disable the "Welcome to Ubuntu..." message.
