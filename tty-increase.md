# Increase the number of tty shells

If you install a default *Ubuntu* distribution on your machine, your graphical shell ships with six *teletypewriters* (TTYs). If you don not want a graphical shell, you can for instance decide to increase the number of teletypewriters.

In many cases that is easy: if you browse `/dev/` you will notice there are no less that 64 teletypewriters defined. The only problem is that these are not activated nor bounded to a <kbd>Ctrl</kbd>+<kbd>Alt</kbd>+<kbd>F8</kbd> or other F-key. In order to initialize enough teletypewriters you can add new `tty.conf` files in `/etc/init/`.

You can simply reuse the other `tty` files in `/etc/init/`. Therefore we can develop a simple one-liner that makes use of `sed`:

    cd /etc/init/
    sed 's/6/7/g' tty6.conf | sudo sponge tty7.conf

`sed` uses the `tty6.conf` file in `/etc/init/` and replaces the `6` with `7` and using `sponge` you save it to `tty7.conf`. Since `/etc/init/` is a folder owned by `root`, you need to use `sudo`.

## Multiple teletypewriters at once

If one wishes to create a large number of additional teletypewriters, for instance up to <kbd>Ctrl</kbd>+<kbd>Alt</kbd>+<kbd>F12</kbd>, you can use a `for` loop and `seq`:

    cd /etc/init/
    for ti in `seq 7 12`
    do
        sed "s/6/$ti/g" tty6.conf | sudo sponge "tty$ti.conf"
    done
