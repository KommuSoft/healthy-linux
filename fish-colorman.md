# Introduce color in manpages with `fish`

One can make the `man` pages more useful by introducing colors. This can make it easier to detect flags, sections, etc.

If one uses fish, one can modify the `~/.config/fish/config.fish` file and add the following lines of code:

    set le (echo -e "\e[0m")              # end all
    set lmb (echo -e "\e[01;31m")         # begin blinking
    set lmd (echo -e "\e[01;31m")         # begin bold
    set lso (echo -e "\e[01;44;33m")      # begin standout-mode - info box
    set lus (echo -e "\e[01;32m")         # begin underline

    set -xU LESS_TERMCAP_mb "$lmb"      # begin blinking
    set -xU LESS_TERMCAP_md "$lmd"      # begin bold
    set -xU LESS_TERMCAP_me "$le"       # end mode
    set -xU LESS_TERMCAP_se "$le"       # end standout-mode
    set -xU LESS_TERMCAP_so "$lso"      # begin standout-mode - info box
    set -xU LESS_TERMCAP_ue "$le"       # end underline
    set -xU LESS_TERMCAP_us "$lus"      # begin underline

The first block defines temporary variables. These variables contain *escape sequences*. These escape sequences will be emitted by `less` (the default pager of `man`). The declarations are only used to make reuse of constant easier.

The next block then binds the content of these variables to global variables each  with `LESS_TERMCAP_*`; these correspond to variables `less` uses to format data.
