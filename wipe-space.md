# Wiping the file system

Most filesystems do not delete a file completely when you use `rm`: they only mark the spaces the file took as free and remove the file from the table of inode such that the file is no longer part of the directory it belonged to. This approach has much benefits: it makes removing files increadibly fast and furthermore it allows one to (partly) recover files if they have been deleted recently (and no other file used the diskspace that was marked free).

Hackers however find this approach useful as well: they can recover files and look for sensitive data like credit card numbers. One can protect him/herself by *wiping* the filesystem: overwriting the diskspace that was marked free with random data or zeros, such that the data can no longer be recovered.

One can use the `secure-delete` package to remove files safely as well as filling up diskspace marked free with noise. When one wants to remove a file securely, one can use:

    srm file

Although a user can always use `srm` and modify his/her shell scripts such that these use secure remove as well, most programs will still the original remove method. Furthermore `srm` is quite inefficient: it is possible the recovered diskspace is immediately claimed by another file that will remove (part of) the content of that file anyway.

The package offers another command as well: `sfill`. `sfill` fills the free space by creating a file called `oooooooo.ooo` and fill it with data. Since the file will keep growing until all available disk space (or quota) is used, it will claim the space that was marked free after removing files. After the process the file `oooooooo.ooo` is removed such that one can reclaim the diskspace. Furthermore it will construct a large number of files to remove remaining inode data: this potentially contains the name of removed files as well as access rights and modification dates).

One can use `sfill` both in user mode or with root access:

    sudo sfill .

for instance in a home directory. A problem I have noticed is that the program sometimes fails to remove all created temporary files (second step). Therefore it is perhaps better to run this in the `/tmp` folder:

    sudo sfill /tmp/

The program furthermore offers flags to speed up the process.

Now it comes down to run this command on a regular basis. To make it easier one can add a cronjob (`crontab -e`) to wipe the file system: 

    # m   h  dom mon dow   command
    13    03 */5 */3 *     bash -c 'sfill /tmp/'
