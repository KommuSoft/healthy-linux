# Colored `date` using fish

The `date` command prints a string with the current date. For example:

    > date
    Tue Oct  6 15:01:00 CEST 2015

The output is not colored: one has to interpret the string fully before one can make conclusions.

## The `datec` function

When using `fish`, one can define a function (in `~/.config/fish/functions/`) by creating a file `datec.fish` with the following content:

    function datec
        echo -e (date +"\e[33m%a \e[31m%b \e[33m%d \e[32m%H:%M:%S \e[34m%Z \e[31m%Y\e[0m" $argv);
    end

The function first executes the `date` command with the given `$argv` arguments: this allows to attach additional flags to the `datec` command that are passed to the inner `date` command.

The format string is however modified: instead of the usual `"%a %b %d %H:%M:%S %Z %Y"`, it is altered with escape sequences of the form `\e[xxm` with `xx` ANSI ecape codes. For instance `33` sets the text color to yellow. `0` ends all modifications. When using this format string, the string itself does not contain escape characters: it simply prints the backslash followed by `e` and other characters. In order to *escape* the string, we use `echo -e` that will escape the string resulting in colored output.

## ANSI terminal

A terminal that supports ANSI escape codes is required.

## TODO


Take into account a given formatting string in `$argv`. Furthermore the trick with `echo -e` is not fully trustworthy, since it is possible that date will emit other escape sequences as well that should remain untouched.
