# Fish completions

Fish can analyze `man` pages and find the options (as well as possible value) that you can use with your commands. By calling the command `fish_update_completions`, `fish` will analyze all `man` pages. Note that you can only run this command in the `fish` shell: `bash` for instance will not understand this command.

# Updating the list of completions

A potential problem is that a user tends to forget to update these completions regularly. Since updates as well as installing new packages results in new/deprecated options; the list of completions will be less helpfull.

Instead of doing the completions oneself, one can use `cron` and set a cronjob that updates the list of completions on a daily (or other) basis. Run `crontab -e` and add the following line to the file:

    13 02 * * * fish -c 'fish_update_completions' >/dev/null 2>/dev/null

Here the list of completions will be updated daily at 02h13. Furthermore the `>/dev/null` and `2>/dev/null` are used to make sure no output will appear on the `stdout` and `stderr`; otherwise this will result in a large amount of error reporting for this cronjob.
